package microservice_template.composite.config;

import lombok.Data;
import org.springframework.http.HttpMethod;

@Data
public class Uri {
    private String uri;
    private HttpMethod httpMethod;
    private boolean debugMode;
}
