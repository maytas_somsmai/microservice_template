package microservice_template.user.repository;

import microservice_template.user.dao.UserStuffEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserStuffRepository extends JpaRepository<UserStuffEntity, Long>, QuerydslPredicateExecutor<UserStuffEntity> {
}
