package microservice_template.user.dao.mapper;

import microservice_template.common.dto.UserDTO;
import microservice_template.user.dao.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class UserMapper {

    @Autowired
    private UserStuffMapper userStuffMapper;

    public UserDTO toDTO(UserEntity entity) {
        return UserDTO.builder()
                .serviceId(entity.getServiceId())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .userStuff(
                        entity.getUserStuffEntity().stream().map(us -> userStuffMapper.toDTO(us)).collect(Collectors.toSet())
                )
                .build();
    }

    public UserEntity toDAO(UserDTO dto) {
        return UserEntity.builder()
                .serviceId(dto.getServiceId())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .build();
    }

    public UserEntity updateDAO(UserEntity dao, UserDTO dto) {
        dao.setFirstName(dto.getFirstName());
        dao.setLastName(dto.getLastName());
        return dao;
    }
}
