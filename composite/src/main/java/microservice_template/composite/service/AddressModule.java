package microservice_template.composite.service;

import microservice_template.common.constants.AddressType;
import microservice_template.common.dto.AddressDTO;

public interface AddressModule {
    AddressDTO getAddress(String serviceId, AddressType main);
}
