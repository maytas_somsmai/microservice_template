package microservice_template.composite.common;

public class RoutingKeyUtil {

    private static final String USER_ROUTING_KEY = "user.%s";
    private static final String ADDRESS_ROUTING_KEY = "user.%s.address";

    public static String userRoutingKey(String serviceId){
        return String.format(USER_ROUTING_KEY, serviceId);
    }

    public static String addressRoutingKey(String serviceId){
        return String.format(ADDRESS_ROUTING_KEY, serviceId);
    }

}
