package microservice_template.composite.service;

import microservice_template.common.dto.UserDTO;

public interface UserModule {
    UserDTO getUserDetail(String serviceId);
}
