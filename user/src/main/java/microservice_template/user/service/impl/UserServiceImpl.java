package microservice_template.user.service.impl;


import com.querydsl.core.types.Predicate;
import lombok.extern.log4j.Log4j2;
import microservice_template.common.dto.UserDTO;
import microservice_template.user.dao.QUserEntity;
import microservice_template.user.dao.UserEntity;
import microservice_template.user.dao.mapper.UserMapper;
import microservice_template.user.repository.UserRepository;
import microservice_template.user.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import java.security.InvalidParameterException;

@Log4j2
@Service
public class UserServiceImpl implements UserService {

    private final String serviceIdPrefix = "01";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    @Qualifier("jdbcScheduler")
    private Scheduler jdbcScheduler;

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDTO getUserProfile(String serviceId) {
        Predicate predicate = QUserEntity.userEntity.serviceId.eq(serviceId);
        UserEntity dao = userRepository.findOne(predicate).orElseThrow(() -> new InvalidParameterException("Not found user"));
        return userMapper.toDTO(dao);
    }

    @Override
    public Page<UserDTO> getUserList(String searchText, Pageable pageable) {
        Predicate predicate = QUserEntity.userEntity.serviceId.likeIgnoreCase("%" + searchText + "%")
                .or(QUserEntity.userEntity.firstName.likeIgnoreCase("%" + searchText + "%"))
                .or(QUserEntity.userEntity.lastName.likeIgnoreCase("%" + searchText + "%"));
        return userRepository.findAll(predicate, pageable).map(pageDao ->
                userMapper.toDTO(pageDao));
    }

    @Override
    public Mono<UserDTO> createUserRx(UserDTO userDTO) {
        return Mono.fromCallable(() -> transactionTemplate.execute(status -> {
            UserEntity dao = userRepository.save(userMapper.toDAO(userDTO));
            dao.setServiceId(generateServiceId(dao.getId()));
            dao = userRepository.save(dao);
            return userMapper.toDTO(dao);
        })).subscribeOn(jdbcScheduler);
    }

    @Override
    @Transactional
    public UserDTO createUser(UserDTO userDTO) {
        UserEntity dao = userRepository.save(userMapper.toDAO(userDTO));
        dao.setServiceId(generateServiceId(dao.getId()));
        dao = userRepository.save(dao);
        return userMapper.toDTO(dao);
    }

    @Override
    @Transactional
    public UserDTO updateUserProfile(UserDTO userDTO) {
        Predicate predicate = QUserEntity.userEntity.serviceId.eq(userDTO.getServiceId());
        UserEntity dao = userRepository.findOne(predicate).orElseThrow(() -> new InvalidParameterException("Not found user"));
        dao = userRepository.save(userMapper.updateDAO(dao, userDTO));
        return userMapper.toDTO(dao);
    }

    private String generateServiceId(Long id) {
        return String.format("%s%s",
                serviceIdPrefix,
                StringUtils.leftPad(id.toString(), 5, "0")
        );
    }
}