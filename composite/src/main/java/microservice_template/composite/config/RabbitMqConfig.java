package microservice_template.composite.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.springframework.amqp.core.BindingBuilder.bind;

//@Configuration
public class RabbitMqConfig {

    private final String USER_QUEUE = "user.queue";
    private final String ADDRESS_QUEUE = "address.queue";
    public static final String USER_TOPIC = "user.topic";

    @Bean
    public Declarables topicBindings() {
        Queue userQueue = new Queue(USER_QUEUE, false);
        Queue addressQueue = new Queue(ADDRESS_QUEUE, false);

        TopicExchange userTopic = new TopicExchange(USER_TOPIC);

        return new Declarables(
                userQueue,
                addressQueue,
                userTopic,
                bind(userQueue)
                        .to(userTopic).with("use.*"),
                bind(addressQueue)
                        .to(userTopic).with("user.*.address"));
    }

}
