package microservice_template.address.service.impl;


import com.querydsl.core.types.Predicate;
import microservice_template.address.dao.AddressEntity;
import microservice_template.address.dao.QAddressEntity;
import microservice_template.address.dao.mapper.AddressMapper;
import microservice_template.address.repository.AddressRepository;
import microservice_template.address.service.AddressService;
import microservice_template.common.constants.AddressType;
import microservice_template.common.dto.AddressDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.InvalidParameterException;

@Service
public class AddressServiceImpl implements AddressService{

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AddressMapper addressMapper;


    @Override
    public Page<AddressDTO> getAddress(String serviceId, AddressType addressType, Pageable pageable) {
        Predicate predicate = QAddressEntity.addressEntity.serviceId.eq(serviceId).and(QAddressEntity.addressEntity.type.eq(addressType));
        return addressRepository.findAll(predicate, pageable).map(daoContent -> addressMapper.toDTO(daoContent));
    }

    @Override
    @Transactional
    public AddressDTO createAddress(String serviceId, AddressDTO addressDTO) {
        AddressEntity dao = addressRepository.save(addressMapper.toDAO(serviceId, addressDTO));
        return addressMapper.toDTO(dao);
    }

    @Override
    @Transactional
    public AddressDTO updateAddress(String serviceId, Long addressId, AddressDTO addressDTO) {
        Predicate predicate = QAddressEntity.addressEntity.id.eq(addressId).and(QAddressEntity.addressEntity.serviceId.eq(serviceId));
        AddressEntity dao = addressRepository.findOne(predicate).orElseThrow(() -> new InvalidParameterException("Not found"));
        dao = addressMapper.updateDAO(dao, addressDTO);
        addressRepository.save(dao);
        return addressMapper.toDTO(dao);
    }
}