package microservice_template.composite.service.impl;

import microservice_template.common.constants.AddressType;
import microservice_template.common.dto.AddressDTO;
import microservice_template.common.util.UrlBuilderUtil;
import microservice_template.composite.common.InternalRestTemplate;
import microservice_template.composite.config.UriConfig;
import microservice_template.composite.service.AddressModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import static microservice_template.composite.config.APINames.*;

@Service
public class AddressModuleImpl implements AddressModule {


    private final String BASE_URL;

    private UriConfig urlConfig;
    private InternalRestTemplate internalRestTemplate;
    private UrlBuilderUtil urlBuilderUtil;

    @Autowired
    public AddressModuleImpl(UriConfig urlConfig, InternalRestTemplate internalRestTemplate, UrlBuilderUtil urlBuilderUtil) {
        this.urlConfig = urlConfig;
        this.internalRestTemplate = internalRestTemplate;
        this.urlBuilderUtil = urlBuilderUtil;
        this.BASE_URL = this.urlConfig.getModuleBaseUri(ADDRESS_MODULE);
    }

    @Override
    public AddressDTO getAddress(String serviceId, AddressType type) {
        Map<String, String> pathParams = new HashMap<>();
        pathParams.put("serviceId", serviceId);
        pathParams.put("type", type.toString());

        RestTemplate restTemplate = internalRestTemplate.initInternalRestTemplate();
        HttpEntity httpEntity = internalRestTemplate.initInternalHttpEntity();
        URI uri = urlBuilderUtil.buildUriComponents(BASE_URL + urlConfig.URIs.get(GET_ADDRESS).getUri(), pathParams);

        ResponseEntity<AddressDTO> response = restTemplate.exchange(uri, urlConfig.URIs.get(GET_ADDRESS).getHttpMethod(), httpEntity, AddressDTO.class);

        return response.getBody();
    }

}