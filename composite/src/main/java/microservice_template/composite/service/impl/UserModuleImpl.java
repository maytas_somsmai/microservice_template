package microservice_template.composite.service.impl;

import microservice_template.common.dto.UserDTO;
import microservice_template.common.util.UrlBuilderUtil;
import microservice_template.composite.common.InternalRestTemplate;
import microservice_template.composite.config.UriConfig;
import microservice_template.composite.service.UserModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import static microservice_template.composite.config.APINames.GET_USER_DETAIL;
import static microservice_template.composite.config.APINames.USER_MODULE;

@Service
public class UserModuleImpl implements UserModule {


    private final String BASE_URL;

    private UriConfig urlConfig;
    private InternalRestTemplate internalRestTemplate;
    private UrlBuilderUtil urlBuilderUtil;

    @Autowired
    public UserModuleImpl(UriConfig urlConfig, InternalRestTemplate internalRestTemplate, UrlBuilderUtil urlBuilderUtil) {
        this.urlConfig = urlConfig;
        this.internalRestTemplate = internalRestTemplate;
        this.urlBuilderUtil = urlBuilderUtil;
        this.BASE_URL = this.urlConfig.getModuleBaseUri(USER_MODULE);
    }

    @Override
    public UserDTO getUserDetail(String serviceId) {
        Map<String, String> pathParams = new HashMap<>();
        pathParams.put("serviceId", serviceId);

        RestTemplate restTemplate = internalRestTemplate.initInternalRestTemplate();
        HttpEntity httpEntity = internalRestTemplate.initInternalHttpEntity();
        URI uri = urlBuilderUtil.buildUriComponents(BASE_URL + urlConfig.URIs.get(GET_USER_DETAIL).getUri(), pathParams);

        ResponseEntity<UserDTO> response = restTemplate.exchange(uri, urlConfig.URIs.get(GET_USER_DETAIL).getHttpMethod(), httpEntity, UserDTO.class);

        return response.getBody();
    }
}