package microservice_template.composite.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Data
@Component
@ConfigurationProperties(prefix = "application.internal-uris", ignoreUnknownFields = false)
public class UriConfig {

    public HashMap<APINames, Uri> URIs;

    public String getModuleBaseUri(APINames moduleName) {
        return URIs.get(moduleName).getUri();
    }

}