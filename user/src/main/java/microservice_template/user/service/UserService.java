package microservice_template.user.service;


import microservice_template.common.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Mono;

public interface UserService {

    UserDTO getUserProfile(String serviceId);

    Page<UserDTO> getUserList(String searchText, Pageable pageRequest);

    Mono<UserDTO> createUserRx(UserDTO userDTO);

    UserDTO createUser(UserDTO userDTO);

    UserDTO updateUserProfile(UserDTO userDTO);
}