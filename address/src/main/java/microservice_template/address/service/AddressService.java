package microservice_template.address.service;


import microservice_template.common.constants.AddressType;
import microservice_template.common.dto.AddressDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AddressService {

    Page<AddressDTO> getAddress(String serviceId, AddressType addressType, Pageable pageable);

    AddressDTO createAddress(String serviceId, AddressDTO addressDTO);

    AddressDTO updateAddress(String serviceId, Long addressId, AddressDTO addressDTO);
}