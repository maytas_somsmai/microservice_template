package microservice_template.common.util;

import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

@Component
public class UrlBuilderUtil {

    public URI buildUriComponents(String url, Map<String, String> uriParams) {
        return buildUriComponents(url, uriParams, null);
    }

    public URI buildUriComponents(String url, Map<String, String> uriParams, MultiValueMap<String, String> queryParams) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
        URI uri = builder.queryParams(queryParams).buildAndExpand(uriParams).toUri();
        return uri;
    }

}
