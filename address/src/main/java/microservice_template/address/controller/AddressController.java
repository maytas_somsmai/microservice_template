package microservice_template.address.controller;

import microservice_template.address.service.AddressService;
import microservice_template.common.constants.AddressType;
import microservice_template.common.dto.AddressDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = "/v1/address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @GetMapping(path = "/{serviceId}/type/{type}")
    public ResponseEntity<Page<AddressDTO>> getMainAddress(@PathVariable String serviceId,
                                                     @PathVariable AddressType type,
                                                     @PageableDefault(sort = "type", direction = Sort.Direction.ASC) Pageable pageable
    ) {
        return ResponseEntity.ok(addressService.getAddress(serviceId, type, pageable));
    }

}
