package microservice_template.common.constants;

public enum AddressType {
    MAIN, BILLING
}
