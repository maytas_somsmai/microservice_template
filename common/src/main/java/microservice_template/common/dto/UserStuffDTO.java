package microservice_template.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import microservice_template.common.constants.StuffType;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserStuffDTO {
    private StuffType stuffType;
    private String name;
    private String description;
}
