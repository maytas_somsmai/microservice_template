package microservice_template.composite.service;

import microservice_template.composite.dto.UserProfileDTO;
import reactor.core.publisher.Mono;

public interface UserComposite {

    Mono<UserProfileDTO> getUserProfileFlux(String serviceId);
    UserProfileDTO getUserProfile(String serviceId);

}
