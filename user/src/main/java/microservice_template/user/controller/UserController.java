package microservice_template.user.controller;

import lombok.extern.log4j.Log4j2;
import microservice_template.common.dto.UserDTO;
import microservice_template.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@Log4j2
@RestController
@RequestMapping(path = "/v1/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/{serviceId}")
    public UserDTO getUserDetail(@PathVariable String serviceId) {
        return userService.getUserProfile(serviceId);
    }

    @GetMapping
    public Page<UserDTO> getUserList(@RequestParam String searchText, @PageableDefault(page = 0, size = 10) Pageable pageable) {
        return userService.getUserList(searchText, pageable);
    }

    @PostMapping
    public UserDTO createUser(@RequestBody UserDTO userDTO) {
        return userService.createUser(userDTO);
    }

    @PutMapping(path = "/{serviceId}")
    public UserDTO updateUser(@RequestBody UserDTO userDTO, @PathVariable String serviceId) {
        userDTO.setServiceId(serviceId);
        return userService.updateUserProfile(userDTO);
    }
}
