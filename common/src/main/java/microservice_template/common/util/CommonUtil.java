package microservice_template.common.util;

import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.function.Supplier;

@Component
public class CommonUtil {
    public Optional resolveNestedObj(Supplier supplier) {
        Optional result;
        try {
            result = Optional.ofNullable(supplier.get());
        } catch (NullPointerException e) {
            result = Optional.empty();
        }
        return result;
    }
}
