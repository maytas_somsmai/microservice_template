package microservice_template.composite.config;

public enum APINames {
    USER_MODULE, GET_USER_DETAIL,

    ADDRESS_MODULE, GET_ADDRESS,
}
