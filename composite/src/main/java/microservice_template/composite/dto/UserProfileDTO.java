package microservice_template.composite.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import microservice_template.common.dto.AddressDTO;
import microservice_template.common.dto.UserDTO;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserProfileDTO {
    private UserDTO user;
    private AddressDTO mainAddress;
    private AddressDTO billingAddress;
}
