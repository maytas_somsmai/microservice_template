package microservice_template.user.service;


import microservice_template.common.dto.UserStuffDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface UserStuffService {

    Page<UserStuffDTO> findUserStuff(String serviceId, String searchText, Pageable pageRequest);

    UserStuffDTO createUserStuff(String serviceId, UserStuffDTO userDTO);

    UserStuffDTO updateUserStuff(String serviceId, UserStuffDTO userDTO);
}