package microservice_template.user.controller;

import lombok.extern.log4j.Log4j2;
import microservice_template.common.dto.UserStuffDTO;
import microservice_template.user.service.UserStuffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RestController
@RequestMapping(path = "/v1/user")
public class UserStuffController {

    @Autowired
    private UserStuffService userStuffService;

    @GetMapping(path = "/{serviceId}/stuff")
    public Page<UserStuffDTO> findUserStuff(
            @PathVariable String serviceId,
            @RequestParam String searchText,
            @PageableDefault(sort = {"stuffType", "name"}) Pageable pageable
    ) {
        return userStuffService.findUserStuff(serviceId, searchText, pageable);
    }

}
