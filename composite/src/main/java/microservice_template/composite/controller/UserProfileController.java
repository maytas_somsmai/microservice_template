package microservice_template.composite.controller;

import microservice_template.composite.dto.UserProfileDTO;
import microservice_template.composite.service.UserComposite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/v1/user")
public class UserProfileController {

    @Autowired
    private UserComposite userComposite;

    @GetMapping(path = "/{serviceId}/rx")
    public Mono<UserProfileDTO> getUserProfileWebflux(@PathVariable String serviceId) {
        return userComposite.getUserProfileFlux(serviceId);
    }

    @GetMapping(path = "/{serviceId}")
    public UserProfileDTO getUserProfileRestTemplate(@PathVariable String serviceId) {
        return userComposite.getUserProfile(serviceId);
    }

}
