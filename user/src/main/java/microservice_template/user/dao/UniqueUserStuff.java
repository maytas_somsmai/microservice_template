package microservice_template.user.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import microservice_template.common.constants.StuffType;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UniqueUserStuff implements Serializable {
    private Long id;
    private StuffType stuffType;
    private String name;
}
