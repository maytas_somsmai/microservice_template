package microservice_template.address.repository;

import microservice_template.address.dao.AddressEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<AddressEntity, Long>, QuerydslPredicateExecutor<AddressEntity> {
}
