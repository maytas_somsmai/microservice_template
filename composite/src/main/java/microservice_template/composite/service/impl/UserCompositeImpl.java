package microservice_template.composite.service.impl;

import microservice_template.common.constants.AddressType;
import microservice_template.common.dto.AddressDTO;
import microservice_template.common.dto.UserDTO;
import microservice_template.composite.dto.UserProfileDTO;
import microservice_template.composite.service.AddressModule;
import microservice_template.composite.service.UserComposite;
import microservice_template.composite.service.UserModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class UserCompositeImpl implements UserComposite {

    @Autowired
    private UserModule userModule;
    @Autowired
    private AddressModule addressModule;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public Mono<UserProfileDTO> getUserProfileFlux(String serviceId) {
        WebClient client = WebClient.create();
        Mono<UserDTO> user = client.get().uri("http://localhost:30002/v1/user/{serviceId}", serviceId).retrieve().bodyToMono(UserDTO.class);
        Mono<AddressDTO> mainAddress = client.get().uri("http://localhost:30003/v1/address/{serviceId}/type/{type}", serviceId, AddressType.MAIN).retrieve().bodyToMono(AddressDTO.class);
        Mono<AddressDTO> billingAddress = client.get().uri("http://localhost:30003/v1/address/{serviceId}/type/{type}", serviceId, AddressType.BILLING).retrieve().bodyToMono(AddressDTO.class);
        return Mono.zip(user, mainAddress, billingAddress).map( zip ->
                UserProfileDTO.builder().user(zip.getT1()).mainAddress(zip.getT2()).billingAddress(zip.getT3()).build()
        );
    }

    @Override
    public UserProfileDTO getUserProfile(String serviceId) {
        UserDTO user = userModule.getUserDetail(serviceId);
        AddressDTO mainAddress = addressModule.getAddress(serviceId, AddressType.MAIN);
        AddressDTO billingAddress = addressModule.getAddress(serviceId, AddressType.BILLING);
        return UserProfileDTO.builder().user(user).mainAddress(mainAddress).billingAddress(billingAddress).build();
    }

}