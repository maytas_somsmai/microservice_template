package microservice_template.user.service.impl;

import microservice_template.common.dto.UserStuffDTO;
import microservice_template.user.service.UserStuffService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserStuffServiceImpl implements UserStuffService {
    @Override
    public Page<UserStuffDTO> findUserStuff(String serviceId, String searchText, Pageable pageRequest) {
        return null;
    }

    @Override
    public UserStuffDTO createUserStuff(String serviceId, UserStuffDTO userDTO) {
        return null;
    }

    @Override
    public UserStuffDTO updateUserStuff(String serviceId, UserStuffDTO userDTO) {
        return null;
    }
}
