package microservice_template.user.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import microservice_template.common.constants.StuffType;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_stuff")
@IdClass(UniqueUserStuff.class)
public class UserStuffEntity implements Serializable{

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private StuffType stuffType;

    private String name;

    @Column
    private String description;

    @ManyToOne
    @JoinColumn(name = "user_id")
    UserEntity userEntity;
}
