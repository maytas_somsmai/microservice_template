package microservice_template.composite.common;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Component
public class InternalRestTemplate {

    public RestTemplate initInternalRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.setErrorHandler();
        return restTemplate;
    }

    public HttpEntity initInternalHttpEntity() {
        return initInternalHttpEntity(null);
    }

    public HttpEntity initInternalHttpEntity(Object requestBody) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return new HttpEntity(requestBody, headers);
    }
}
