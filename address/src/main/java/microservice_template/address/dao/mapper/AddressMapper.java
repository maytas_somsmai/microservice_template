package microservice_template.address.dao.mapper;

import microservice_template.address.dao.AddressEntity;
import microservice_template.common.dto.AddressDTO;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {

    public AddressDTO toDTO(AddressEntity dao) {
        return AddressDTO.builder()
                .type(dao.getType())
                .address(dao.getAddress())
                .build();
    }

    public AddressEntity toDAO(String serviceId, AddressDTO dto) {
        return AddressEntity.builder()
                .serviceId(serviceId)
                .type(dto.getType())
                .address(dto.getAddress())
                .build();
    }

    public AddressEntity updateDAO(AddressEntity dao, AddressDTO dto){
        dao.setAddress(dto.getAddress());
        dao.setType(dto.getType());
        return dao;
    }
}
