package microservice_template.user.dao.mapper;

import microservice_template.common.dto.UserDTO;
import microservice_template.common.dto.UserStuffDTO;
import microservice_template.user.dao.UserEntity;
import microservice_template.user.dao.UserStuffEntity;
import org.springframework.stereotype.Component;

@Component
public class UserStuffMapper {

    public UserStuffDTO toDTO(UserStuffEntity entity) {
        return UserStuffDTO.builder()
                .stuffType(entity.getStuffType())
                .name(entity.getName())
                .description(entity.getDescription())
                .build();
    }

    public UserStuffEntity toEntity(UserStuffDTO dto) {
        return UserStuffEntity.builder()
                .stuffType(dto.getStuffType())
                .name(dto.getName())
                .description(dto.getDescription())
                .build();
    }

    public UserStuffEntity updateEntity(UserStuffEntity entity, UserStuffDTO dto){
        entity.setStuffType(dto.getStuffType());
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        return entity;
    }
}
